---
title: "{{ replace .Name "-" " " | title }}"
weblink: ""
journal: ""
coauthors: ""
date: {{ .Date }}
withcoauthors: true
other: false
preprint: true
draft: true
---
