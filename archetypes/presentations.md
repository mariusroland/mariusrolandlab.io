---
title: "{{ replace .Name "-" " " | title }}"
slidefile: ""
video: ""
otherfiles: ""
event: ""
place: ""
date: {{ .Date }}
videoavailable: false
otherfilesavailable: false
draft: true
---