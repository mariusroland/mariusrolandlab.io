baseURL = "https://mariusroland.gitlab.io/"
languageCode = "en-us"
title = "Marius Roland"
theme = "minimal"

[params]
    author = "Calin Tataru"
    description = ""
    githubUsername = "#"
    accent = "blue"
    showBorder = true
    backgroundColor = "white"
    font = "Raleway" # should match the name on Google Fonts!
    favicon = true
    iconpath = "favicon.png"
    highlight = true
    highlightStyle = "default"
    highlightLanguages = ["go", "haskell", "kotlin", "scala", "swift"]
    profilePic = "rotterdam-crop.jpg"
    OneLineBio = "Research Associate (Chargé de Recherche) at Inria Lille"
    ShortBio = "I studied Mathematical Engineering at the [Université catholique de Louvain](https://uclouvain.be/fr/index.html), Belgium, where I received my Bachelor's degree in 6/2017 and my Master's degree in 6/2019. Afterwards, I received a PhD from [Universität Trier](https://www.uni-trier.de/en/) where I was supervised by Prof. [Martin Schmidt](https://martinschmidt.squarespace.com/about). During my PhD studies my research focused on adaptive refinement algorithms for optimization problems arising in energy transport networks. Following my PhD, I completed a Postdoctoral position at [Polytechnique Montreal](https://www.polymtl.ca/) supervised by Prof. [Thibaut Vidal](https://w1.cirrelt.ca/~vidalt/en/home-thibaut-vidal.html). Starting from January 2025, I am a [CRCN](https://www.inria.fr/en/researchers-normal-class-crcn) (permanent research associate) at [Inria centre at the University of Lille](https://www.inria.fr/en/inria-centre-university-lille). My research interests lie at the interface of optimization and energy systems, with a particular focus on stochastic programming, mixed integer programming, and machine learning approaches. I am especially interested in developing methodologies that bridge the gap between theoretical optimization and practical energy applications, including exact algorithms designed to iteratively solve smaller-size surrogates that approximate more realistic but computationally challenging models."
    Address = "Inria Building B, 6 Rue Heloïse, 59650 Villeneuve-d'Ascq, France"

[[menu.main]]
    url = "/"
    name = "Home"
    weight = 1

[[menu.main]]
    url = "/publications/"
    name = "Publications"
    weight = 2

[[menu.main]]
    url = "/presentations/"
    name = "Presentations"
    weight = 3

[[menu.main]]
    url = "/cv.pdf"
    name = "CV"
    weight = 4

# Social icons to be shown on the right-hand side of the navigation bar.
# The "name" field should match the name of the icon in Font Awesome.
# The list of available icons can be found at http://fontawesome.io/icons.

[[menu.icon]]
    url = "mailto: mmmroland@gmail.com"
    name = "envelope-o"
    weight = 1

[[menu.icon]]
    url = "https://scholar.google.com/citations?hl=fr&user=YZeJ2rIAAAAJ"
    name = "google"
    weight = 2

[[menu.icon]]
    url = "https://gitlab.com/mariusroland"
    name = "gitlab"
    weight = 3

[[menu.icon]]
    url = "https://www.linkedin.com/in/marius-roland-467b2b14a/"
    name = "linkedin"
    weight = 4
