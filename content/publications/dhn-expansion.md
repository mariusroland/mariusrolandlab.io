---
title: "Mixed-Integer Nonlinear Optimization for District Heating Network Expansion"
weblink: "http://www.optimization-online.org/DB_HTML/2020/04/7752.html"
journal: "at - Automatisierungstechnik"
coauthors: "Martin Schmidt"
date: 2020-04-23T14:51:40+02:00
withcoauthors: true
other: false
preprint: false
draft: false
---

