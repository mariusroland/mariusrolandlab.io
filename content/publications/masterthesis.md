---
title: "Analyzing retail pricing options in distribution systems"
weblink: "MasterThesis.pdf"
journal: ""
coauthors: "Anthony Papavasiliou"
type: "Master thesis"
date: 2020-04-23T14:51:40+02:00
withcoauthors: false
preprint: true
other: true
draft: false
---
