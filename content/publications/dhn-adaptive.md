---
title: "Adaptive Nonlinear Optimization of District Heating Networks Based on Model and Discretization Catalogs"
weblink: "http://www.optimization-online.org/DB_HTML/2022/01/8779.html"
journal: "SeMA"
coauthors: "Hannes Dänschel, Volker Mehrmann and Martin Schmidt"
date: 2022-01-27T11:07:24+01:00
withcoauthors: true
other: false
preprint: false
draft: false
---
