---
title: "Exact and Heuristic Solution Techniques for Mixed-Integer Quantile Minimization Problems"
weblink: "http://www.optimization-online.org/DB_HTML/2021/11/8673.html"
journal: "INFORMS Journal on Computing"
coauthors: "Diego Cattaruzza, Martine Labbé, Matteo Petris and Martin Schmidt"
date: 2020-04-23T14:51:40+02:00
withcoauthors: true
other: false
preprint: false
draft: false
---
