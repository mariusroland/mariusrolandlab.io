---
title: "Adaptive Partitioning for Chance-Constrained Problems with Finite Support"
weblink: "https://optimization-online.org/2023/12/adaptive-partitioning-for-chance-constrained-problems-with-finite-support/"
journal: "SIAM Journal on Optimization"
coauthors: "Alexandre Forel, Thibaut Vidal"
date: 2023-12-19T14:51:40+02:00
withcoauthors: true
other: false
preprint: false
draft: false
---
