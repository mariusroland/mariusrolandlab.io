---
title: "Adaptive Refinement Algorithms for Optimization Problems in Energy Transport Networks"
weblink: "phd-thesis.pdf"
journal: ""
coauthors: "Martin Schmidt"
type: "Doctoral thesis"
date: 2022-11-23T14:51:40+02:00
withcoauthors: false
preprint: true
other: true
draft: false
---
