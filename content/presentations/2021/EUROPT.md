---
title: "Mixed-Integer Nonlinear Optimization for District Heating Network Expansion"
slidefile: "EUROPT2021-Marius-Roland.pdf"
video: ""
otherfiles: ""
event: "EUROPT 2021"
place: "ENAC Toulouse"
date: 2021-07-08T15:09:48+02:00
videoavailable: false
otherfilesavailable: false
draft: false
---
