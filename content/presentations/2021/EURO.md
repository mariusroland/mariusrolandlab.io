---
title: "Mixed-Integer Nonlinear Optimization for District Heating Network Expansion"
slidefile: "presEURO.pdf"
video: ""
otherfiles: ""
event: "31st European Conference on Operational Research"
place: "University of West Attica"
date: 2021-07-12T10:41:25+02:00
videoavailable: false
otherfilesavailable: false
draft: false
---
