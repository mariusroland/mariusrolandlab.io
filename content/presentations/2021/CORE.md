---
title: "Adaptive Nonlinear Optimization of District Heating Networks Based on Model and Discretization Hierarchies"
slidefile: "presCORE.pdf"
video: ""
otherfiles: ""
event: "Energy Seminar"
place: "CORE - UCLouvain"
date: 2021-11-04T17:36:31+01:00
videoavailable: false
otherfilesavailable: false
draft: false
---
