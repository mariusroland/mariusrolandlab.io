---
title: "Adaptive Partitioning for Chance-Constrained Problems with Finite Support"
slidefile: "DS4DM.pdf"
video: ""
otherfiles: ""
event: "DS4DM Coffee Talks"
place: "Polytechnique Montréal"
date: 2023-11-05T11:00:00+01:00
videoavailable: false
otherfilesavailable: false
draft: false
---
