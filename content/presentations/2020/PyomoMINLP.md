---
title: "Introduction to the Pyomo Package"
slidefile: "PyomoTut.pdf"
video: ""
otherfiles: "PyomoTut.zip"
event: "Mixed-Integer Nonlinear Optimization Compact Course"
place: "Trier University"
date: 2020-02-18T12:07:09+02:00
videoavailable: false
otherfilesavailable: true
draft: false
---
