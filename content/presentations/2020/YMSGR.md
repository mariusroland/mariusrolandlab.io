---
title: "Mixed-Integer Nonlinear Optimization for District Heating Network Expansion"
slidefile: "YMSGR-Marius-Roland.pdf"
video: ""
otherfiles: ""
event: "Young Mathematicians Symposium of the Greater Region"
place: "Technische Universitaet Kaiserslautern"
date: 2020-09-14T11:41:32+02:00
videoavailable: false
otherfilesavailable: false
draft: false
---