---
title: "Firstart"
date: 2020-04-23T13:50:09+02:00
draft: false
---
[Martin Schmidt](https://martinschmidt.squarespace.com/about) and I finalized our first joint article entitled [Mixed-Integer Nonlinear Optimization for District Heating Network Expansion](http://www.optimization-online.org/DB_HTML/2020/04/7752.html).
