---
title: "EUROSession2021"
date: 2021-07-08T15:58:56+02:00
draft: false
---
I will be chairing the [MINLP for Energy Networks](https://www.euro-online.org/conf/euro31/edit_session?sessid=10) stream at the [EURO 2021 conference](https://euro2021athens.com/).
