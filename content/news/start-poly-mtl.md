---
title: "start-poly-mtl"
date: 2023-05-25T13:50:09+02:00
draft: false
---
I started a Postdoctoral researcher position at [Polytechnique Montreal](https://www.polymtl.ca/) under the supervision of Prof. [Thibaut Vidal](https://w1.cirrelt.ca/~vidalt/en/home-thibaut-vidal.html).
