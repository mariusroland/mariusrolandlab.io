---
title: "COREPresentation2021"
date: 2021-11-04T15:58:56+02:00
draft: false
---
I will be presenting my recent work on [Adaptive Nonlinear Optimization of District Heating Networks Based on Model and Discretization Hierarchies](presentations/presCORE.pdf) at the [CORE - UCLouvain](https://uclouvain.be/en/research-institutes/lidam/core).
