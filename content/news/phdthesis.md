---
title: "phdthesis"
date: 2022-11-07T10:53:21+01:00
draft: False
---
I successfuly defended my PhD entitled [Adaptive Refinement Algorithms for Optimization Problems in Energy Transport Networks](publications/phd-thesis.pdf).
