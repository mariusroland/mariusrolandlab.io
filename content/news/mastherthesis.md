---
title: "Mastherthesis"
date: 2019-06-28T17:00:45+02:00
draft: false
---
 My [Master Thesis](MasterThesis.pdf) has been awarded the second prize of the 2019 [IEEE/ICteam Best Master's Thesis Award](https://sites.uclouvain.be/ieee/2019/05/20/2019-ieee-icteam-best-masters-thesis-award/). 
