---
title: "new-paper"
date: 2024-11-19T13:50:09+02:00
draft: false
---
Our paper [Adaptive Partitioning for Chance-Constrained Problems
with Finite Support](https://optimization-online.org/2023/12/adaptive-partitioning-for-chance-constrained-problems-with-finite-support/) is accepted at SIAM Journal on Optimization.
