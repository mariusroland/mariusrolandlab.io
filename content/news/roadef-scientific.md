---
title: "RoadefScientific"
date: 2022-02-24T10:53:21+01:00
draft: False
---
We (jointly with Diego Cattaruzza, Martine Labbé, Matteo Petris, and Martin Schmidt) have won the scientific prize for the [ROADEF/EURO challenge on grid operation based outage maintenance planning](https://www.roadef.org/challenge/2020/en/sujet.php). 
