---
title: "start-inria"
date: 2025-01-01T13:50:09+02:00
draft: false
---
I started as [CRCN](https://www.inria.fr/en/researchers-normal-class-crcn) (permanent research associate) at [Inria centre at the University of Lille](https://www.inria.fr/en/inria-centre-university-lille).
